import mDump from "../libs/mDump";

export default class Core {
    constructor({ type, host, port, username, password, database, synchronize = false }, entities) {
        this.type = type
        this.host = host
        this.port = port
        this.user = username
        this.password = password
        this.database = database
        this.synchronize = synchronize
        this.entities = entities
    }

    initTables() {
        return
    }

    getType(source) {
        return
    }

    getFields(attributes = []) {
        return (attributes.length == 0) ? '*' : attributes.join(', ')
    }

    async query(query, params) {
        return
    }

    async save(entity, data) {
        return
    }
    async count(entity) {
        return
    }
    async findByPk(entity, id, { attributes }) {
        return
    }
    async findAll(entity, { attributes }) {
        return
    }
    async findOne(entity, where, attributes) {
        return
    }
    async update(entity, data) {
        return
    }
    async remove(entity, data) {
        return
    }

    dump() {
        const { type, host, port, user, password, database } = this;
        mDump(`${type}://${user}:${password}@${host}:${port}/${database}`);
    }
}