let Type = Object.freeze({
    STRING: "string",
    INTEGER: "integer",
    BOOLEAN: "boolean",
    DATE: "date",
    TIME: "time",
})

module.exports = Type