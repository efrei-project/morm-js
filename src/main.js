import mOrm from "./mOrm"
import Student from "./entities/student"

async function init() {
    let orm = new mOrm()

    await orm.createConnection({
        uri: 'postgresql://20180203:@localhost:5432/',
        synchronize: true,
    }, {
        entities: [Student],
    })

    orm.dbInstance.dump()

    const studentEntity = orm.getEntity('Student')


    const student1Saved = await studentEntity.save({
        firstname: 'Sloki',
        lastname: 'Hunt spé Beast'
    })
    console.log(`Inserted value: ${student1Saved.firstname}`)



    // Insert a new Student
    const student2Saved = await studentEntity.save({
        firstname: 'Arthas',
        lastname: 'Lich King'
    })
    console.log(`Inserted value: ${student2Saved.firstname}`)

    
    const findByPkResult = await studentEntity.findByPk(1, {});
    console.log(`Find By Pk: ${findByPkResult.firstname}`);

    const findAllResult = await studentEntity.findAll({});
    console.log(`Find All: ${findAllResult.map(e => e.firstname).join(', ')}`);

    const findOneResult = await studentEntity.findOne({
        where: { firstname: "Sloki" },
        attributes: ["lastname"]
    });
    console.log(`Find one: ${findOneResult.lastname}`)

}

init()