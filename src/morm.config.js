import Student from '../entities/Student'

export default data = {
  config: {
    "type": "postgres",
    "host": "localhost",
    "port": 5432,
    "username": "20180203",
    "password": "",
    "database": "iLovePragmatic"
  },
  entities: [Student]
}