import PostgreSQL from "./engine/postgresql"
let { readFileSync, existsSync } = require('fs')
let { join } = require('path')

export default class mOrm {
    configPathName = "./mOrm.config.json"
    config
    dbInstance

    async createConnection(dbConfig = {}, extras = { entities: [] }, logging = true) {
        this.config = await this.validateConfig(dbConfig)
        this.config.synchronize = dbConfig.synchronize !== undefined ? dbConfig.synchronize : false;

        this.entities = {}
        extras.entities.forEach(e => {
            this.entities[e.name] = e
        })

        this.config.logging = logging

        if (this.config.type == "postgresql") {
            this.dbInstance = await new PostgreSQL(this.config, this.entities)
        } else {
            throw new Error()
        }
        await this.dbInstance.initialize()
    }

    async validateConfig(dbConfig) {
        if (dbConfig.type && dbConfig.host && dbConfig.username && dbConfig.password && dbConfig.database)
            return dbConfig
        if (dbConfig.uri) {
            const regex = /^(.*):\/\/(.*):(.*)@(.*):(\d+)\/(.*)$/g
            const [, type, username, password, host, port, database] = regex.exec(dbConfig.uri)
            return { type, username, password, host, port, database }
        }

        const configFilePath = join(__dirname, this.configPathName);

        if (!existsSync(configFilePath))
            throw new Error()

        let configFile = readFileSync(configFilePath)
        return validateConfig(JSON.parse(configFile), false)
    }

    getEntity(entityName) {
        const entity = this.entities[entityName]
        if (!entity)
            throw (`${entityName} is not a valid model !`)
        return new entity(this.dbInstance)
    }
}